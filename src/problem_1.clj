(ns problem-1)

;;;;
;;Problem 1 Thread-last Operator ->>
;;;;
(def invoice (clojure.edn/read-string (slurp "invoice.edn")))

(defn verify-iva
  "Check if the invoice-item has IVA=19%"
  [invoice-item]
  (->> invoice-item
       (:taxable/taxes)
       (some #(and (= :iva (:tax/category %)) (= 19 (:tax/rate %)))))
  )

(defn verify-retention
  "Check if the invoice-item has retention=1%"
  [invoice-item]
  (->> invoice-item
       (:retentionable/retentions)
       (some #(or (and (= :ret_fuente (:retention/category %)) (= 1 (:retention/rate %)))
                  ;;there are items with key name "retentions" instead of just "retention"
                  (and (= :ret_fuente (:retentions/category %)) (= 1 (:retentions/rate %))))))
  )

(defn find-invoice-items
  "Given an invoice returns invoice-items that satisfy the conditions given"
  [invoice-to-search]
  (->> invoice-to-search
       (:invoice/items)
       (filter #(or (and (verify-iva %) (not (verify-retention %)))
                    (and (verify-retention %) (not (verify-iva %)))))
       )
  )

(def items-found (find-invoice-items invoice))
