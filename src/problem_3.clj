(ns problem-3
  (:require
    [invoice-item :as it]))
(use 'clojure.test)

;;;;
;;Problem 3 Test Driven development
;;;;
(deftest subtotal
  ;;test when invoice-item is complete
  (is (= 5000.0 (it/subtotal #:invoice-item{:precise-quantity 10
                                            :precise-price 1000
                                            :discount-rate 50})))
  ;;test when invoice-item is missing discount-rate
  (is (= 2752.5 (it/subtotal #:invoice-item{:precise-quantity 5
                                             :precise-price 550.5})))
  ;;test when precise-quantity is zero
  (is (= 0.0 (it/subtotal #:invoice-item{:precise-quantity 0
                                         :precise-price 5000
                                         :discount-rate 0})))
  ;;test when :discount-rate is greater than 100
  (is (= -1000.0 (it/subtotal #:invoice-item{:precise-quantity 2
                                         :precise-price 1000
                                         :discount-rate 150})))
  ;;test when :discount-rate is a double
  (is (= 1750.0 (it/subtotal #:invoice-item{:precise-quantity 2
                                             :precise-price 1000
                                             :discount-rate 12.5})))
  ;;test with big :precise-price
  (is (= 1.0E17 (it/subtotal #:invoice-item{:precise-quantity 1000
                                            :precise-price 1.0E14})))
  ;;test when invoice-item is empty
  (is (thrown? NullPointerException (it/subtotal {})))
  )
