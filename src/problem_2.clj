(ns problem-2
  (:require
    [invoice-spec :as is]
    [clojure.spec.alpha :as s]
    [clojure.data.json :as json]))

;;;;
;;Problem 2 Core Generating Functions
;;;;
(defn custom-keyword-namespace
  "Returns a valid keyword namespace to map an invoice.json file"
  [x]
  (case x
    ("price" "quantity" "sku" "taxes") "invoice-item"
    ("tax_category" "tax_rate") "tax"
    ("company_name" "email") "customer"
    "invoice")
  )

(defn custom-keyword
  "Returns a valid keyword to map an invoice.json file"
  [x]
  (case x
    ("tax_category") "category"
    ("tax_rate") "rate"
    ("company_name") "name"
    ("issue_date") "issue-date"
    x)
  )

(defn parse-key
  "Given a key string from the JSON returns a keyword"
  [x]
  (keyword (custom-keyword-namespace x)
           (custom-keyword x))
  )

(defn parse-value
  "Given a key and value from the JSON returns a needed value for spec"
  [key value]
  (case key
    (:tax/category) (keyword (clojure.string/lower-case value))
    (:tax/rate) (double value)
    (:invoice/issue-date) (java.util.Date. value)
    value)
  )

(defn generate-invoice
  "Given a filename returns a map that conforms the spec ::invoice"
  [filename]
  (let [invoice
        (json/read-str (slurp filename)
                       {:key-fn   parse-key
                        :value-fn parse-value})]
    (:invoice/invoice invoice))
  )

(def invoice-object (generate-invoice "invoice.json"))

(defn validate-created-invoice
  "Validates if the invoice conforms to the spec ::invoice"
  [invoice]
  (s/valid? ::is/invoice invoice)
  )